﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace YDR.Extension
{
    public class BaseRewardListener : MonoBehaviour
    {
        [SerializeField] protected RewardEvent _eventToFulfill;
        [SerializeField] protected UnityEvent _onEventRaised;

        private void Start()
        {
            _eventToFulfill.Invoke();
        }

        private void OnEnable()
        {
            _eventToFulfill.Register(this);
        }

        private void OnDisable()
        {
            _eventToFulfill.Unregister(this);
        }

        public virtual void RaiseEvent()
        {
            _onEventRaised.Invoke();
        }

    }
}