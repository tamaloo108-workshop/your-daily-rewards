﻿using System.Collections.Generic;
using UnityEngine;

namespace YDR.Extension
{
    [CreateAssetMenu(fileName = "New Reward Event", menuName = "YourDailyRewards/Create reward event file")]
    public class RewardEvent : ScriptableObject
    {
        HashSet<BaseRewardListener> _listener = new HashSet<BaseRewardListener>();

        public void Invoke()
        {
            foreach (var item in _listener)
            {
                item.RaiseEvent();
            }
        }

        public void Register(BaseRewardListener rewardListener)
        {
            if (!_listener.Contains(rewardListener))
                _listener.Add(rewardListener);
        }

        public void Unregister(BaseRewardListener rewardListener)
        {
            if (_listener.Contains(rewardListener))
                _listener.Remove(rewardListener);
        }
    }
}