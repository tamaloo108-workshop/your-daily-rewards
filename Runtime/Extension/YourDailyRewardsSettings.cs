﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YDR.Core
{
    [CreateAssetMenu(fileName = "YDR Settings", menuName = "YourDailyRewards/Create Your Daily Rewards Settings file")]
    public class YourDailyRewardsSettings : ScriptableObject
    {
        [SerializeField] string lastLoginDate; //for editor purpose only
        [SerializeField] string lastLoginTime; //for editor purpose only

        public string LastLoginDate { get; private set; }
        public string LastLoginTime { get; private set; }

        public void SetLastDateLogin(string currentDateLogin)
        {
            LastLoginDate = currentDateLogin;
            lastLoginDate = LastLoginDate;
        }

        public void SetLastTimeLogin(string currentTimeLogin)
        {
            LastLoginTime = currentTimeLogin;
            lastLoginTime = LastLoginTime;
        }
    }

}