﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace YDR.Extension
{
    public class DelayedRewardListener : BaseRewardListener
    {
        [Header("(Optional) Will be called after event up there.")]
        [SerializeField] float _delay = 1f;
        [SerializeField] UnityEvent _delayedUnityEvent;

        public override void RaiseEvent()
        {
            StartCoroutine(RunDelayedEvent(base.RaiseEvent));
        }

        IEnumerator RunDelayedEvent(UnityAction _lst)
        {
            yield return new WaitForSeconds(_delay);
            _lst.Invoke();
            yield return new WaitForSeconds(_delay);
            _delayedUnityEvent.Invoke();
        }
    }
}