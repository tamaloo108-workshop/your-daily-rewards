﻿using System.Threading.Tasks;
using UnityEngine;
using UTH.Core;
using System.IO;
using UnityEditor;

namespace YDR.Core
{
    public class YDRCore
    {
        const string DEF_LOAD_PATH = "Data/YDR Settings";

        private static YDRCore instance;

        public static YDRCore Instance
        {
            get
            {
                if (instance == null)
                    instance = new YDRCore();
                return instance;
            }
        }

        private YourDailyRewardsSettings _YDRData;
        public YourDailyRewardsSettings YDRData
        {
            get
            {
                if (_YDRData == null)
                {
                    LoadYDRData(DEF_LOAD_PATH);
                }

                return _YDRData;
            }
        }

        static YDRCore()
        {

        }

        public void LoadYDRData(string pathAndName)
        {
            if (File.Exists(pathAndName))
                _YDRData = Resources.Load<YourDailyRewardsSettings>(pathAndName);
            else
            {

#if UNITY_EDITOR
                if (!Directory.Exists("Assets/Resources"))
                {
                    AssetDatabase.CreateFolder("Assets", "Resources");
                }

                if (!Directory.Exists("Assets/Resources/Data"))
                {
                    AssetDatabase.CreateFolder("Assets/Resources", "Data");
                }

                var data = ScriptableObject.CreateInstance<YourDailyRewardsSettings>();
                AssetDatabase.CreateAsset(data, "Assets/Resources/Data/YDR Settings.asset");
                _YDRData = Resources.Load<YourDailyRewardsSettings>(DEF_LOAD_PATH);
#endif
            }

        }

        #region Daily Login Event

        public string lastDateLogin { get => YDRData.LastLoginDate; set => lastDateLogin = YDRData.LastLoginDate; }
        public string currentDateLogin { get; private set; }
        public string lastTimeLogin { get => YDRData.LastLoginTime; set => lastTimeLogin = YDRData.LastLoginTime; }
        public string currentTimeLogin { get; private set; }

        public async Task GetDateTimeNow()
        {
            await GetDateTimeNowAsync();
        }

        private async Task GetDateTimeNowAsync()
        {
            await TimeCore.GetServerDate();
            await TimeCore.GetServerTime();

            currentDateLogin = TimeCore.currentDate;
            currentTimeLogin = TimeCore.currentTime;
        }

        public void SyncLoginDate()
        {
            YDRData.SetLastDateLogin(currentDateLogin);
        }

        public void SyncLoginTime()
        {
            YDRData.SetLastTimeLogin(currentTimeLogin);
        }

        #endregion

    }
}