﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YDR.Core;
using YDR.Extension;

namespace YDR.Example
{
    public class YDRDailyRewards : MonoBehaviour
    {
        public async void GetDateLogin()
        {
            Debug.Log("hmmm");
            await YDRCore.Instance.GetDateTimeNow();
            ClaimReward();
        }

        internal void ClaimReward()
        {
            if (string.Equals(YDRCore.Instance.YDRData.LastLoginDate, YDRCore.Instance.currentDateLogin))
            {
                Debug.Log("already claimed, should return now");
                YDRCore.Instance.SyncLoginDate();
                YDRCore.Instance.SyncLoginTime();
                return;
            }

            YDRCore.Instance.SyncLoginDate();
            YDRCore.Instance.SyncLoginTime();
            Debug.Log("you claim reward");
        }
    }
}